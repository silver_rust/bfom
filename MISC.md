# Useful info
#### Because I am an iot who would forget it otherwise

## Update Crates
``cargo smart-release -u --no-publish --no-changelog-preview bfom bfom-lib bfom-blog``

## Where I want to take this repo.
Splitting off the library before was a good idea, going to extend that further.  
I am going to split off a new binary for my own site use while making teh current bfom to be a general converter.  
This would allow me to make changes to suit my own needs while still keeping teh functionality of the core package to being a better markdown converter.

I also want/need to improve the lib