{
	description = "Brendan's Flavor of Markdown";
  
  inputs = {
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
  };

  outputs = { self, nixpkgs, utils, naersk }: utils.lib.eachDefaultSystem (system: 
    let
      pkgs = nixpkgs.legacyPackages."${system}";
      naersk-lib = naersk.lib."${system}";
      package_name = "bfom";
    in rec {

      # "${self.defaultPackage."${system}"}/bin/cargo-bfom"
      # "${self.defaultPackage."${system}"}/bin/cargo-bfom-blog"
      
      # `nix build`
      packages."${package_name}" = naersk-lib.buildPackage {
        pname = "${package_name}";
        root = ./.;
      };

      defaultPackage = packages."${package_name}";

      # `nix run`
      apps."${package_name}" = utils.lib.mkApp {
        drv = packages."${package_name}";
      };

      defaultApp = apps."${package_name}";

      # `nix develop`
      devShell = pkgs.mkShell {
        nativeBuildInputs = with pkgs; [ rustc cargo ];
      };
      
    });
}