# Brendan's Flavor of Markdown
##### Library

## About 
This is the library that powers my [BFoM] converter.  
Since this was created in part to expand my knowledge of rust it has no external dependencies.

## Useage
To use in your own project add it to the Cargo.toml:
````toml
[dependencies]
bfom-lib = "0.1"
````

````rust
use bfom_lib::{Config, Converter, Template};

fn main () -> Result<(), std::io::Error> {
    // new uses inbuilt defaults if its passed None
    let mut converter = Converter::new(None);
    converter.convert()?;
}

````
If you want to override the defaults please have a look at [BFoM].


[BFoM]: ../bfom 