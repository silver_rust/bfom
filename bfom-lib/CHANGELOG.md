# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## v0.1.52 (2024-08-12)

### New Features

 - <csr-id-7fdd6ea2b52f9bda6eee4d7d1cb40c79f549c8e4/> allow use of a non numeric slug

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 1 commit contributed to the release.
 - 1 commit was understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Allow use of a non numeric slug (7fdd6ea)
</details>

## v0.1.51 (2024-08-11)

### New Features

 - <csr-id-7454a07696fd05cadceca05f34b447383b9e9741/> main program can now do the index functions

### Bug Fixes

 - <csr-id-fc1d9548edf4ca5e26dabdf676e2b9a1fca448f0/> underflows are painful
 - <csr-id-34ae4439f2133c52d49b326b8f1547c649c6126a/> for publishing
 - <csr-id-4d9114f672ca420630d9d617715c5e3d189bc4d8/> for publishing
 - <csr-id-f14d9ab23ab5d72fef358743fe1966108e343f55/> for publishing

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 9 commits contributed to the release over the course of 7 calendar days.
 - 104 days passed between releases.
 - 5 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.51 (fffd69b)
    - For publishing (f14d9ab)
    - Bump bfom-lib v0.1.51 (ee68813)
    - For publishing (4d9114f)
    - Bump bfom-lib v0.1.51 (c4e3bd3)
    - For publishing (34ae443)
    - Bump bfom-lib v0.1.51, bfom v0.1.44 (f7eaeaf)
    - Underflows are painful (fc1d954)
    - Main program can now do the index functions (7454a07)
</details>

## v0.1.50 (2024-04-28)

### Bug Fixes

 - <csr-id-45ef8b1b9f91374101e5ba2014c9f59c3057dfbd/> can handle both types with this now
 - <csr-id-8b6ef0a7a15b9ca4d7ef0764b6e43b7eb72b6e55/> now only cares if they are both OL
 - <csr-id-7c6d04a8412be725a5f51bc639969dc40eeffce1/> now only cares if theya re both OL's

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 4 commits contributed to the release.
 - 3 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.50, bfom-blog v0.1.21 (664e537)
    - Can handle both types with this now (45ef8b1)
    - Now only cares if they are both OL (8b6ef0a)
    - Now only cares if theya re both OL's (7c6d04a)
</details>

## v0.1.49 (2024-04-28)

### Bug Fixes

 - <csr-id-5d1f75dddb2d4b6c828184b158615ee51f9a4350/> bit of a bodge to render lists correctly

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 2 commits contributed to the release.
 - 216 days passed between releases.
 - 1 commit was understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.49 (2ef52e1)
    - Bit of a bodge to render lists correctly (5d1f75d)
</details>

## v0.1.48 (2023-09-24)

<csr-id-4ecc336d3b776f96cb976f46b7079a986bdf1609/>

### Other

 - <csr-id-4ecc336d3b776f96cb976f46b7079a986bdf1609/> I like having two spaces isntead of tabs

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 2 commits contributed to the release.
 - 145 days passed between releases.
 - 1 commit was understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.48, bfom v0.1.41, bfom-blog v0.1.20 (12301d7)
    - I like having two spaces isntead of tabs (4ecc336)
</details>

## v0.1.47 (2023-05-02)

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 2 commits contributed to the release.
 - 17 days passed between releases.
 - 0 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Adjusting changelogs prior to release of bfom-lib v0.1.47 (d74f4d3)
    - Lines with html tags preceded by whitespace were not getting identified correctly. (6adff9d)
</details>

## v0.1.46 (2023-04-15)

### New Features

 - <csr-id-465b80e87e3d57a8ecde4c92fb0ef0e96e383911/> can now have quotes on strins in the frontmatter.
   Should stop intellij yelling at me
 - <csr-id-47f21328aae4aadb3a40564acbf5f73fe77c58a1/> can now get certain blog entries to not show up on teh index

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 3 commits contributed to the release.
 - 2 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.46, bfom-blog v0.1.18 (09e5fd8)
    - Can now have quotes on strins in the frontmatter. (465b80e)
    - Can now get certain blog entries to not show up on teh index (47f2132)
</details>

## v0.1.45 (2023-04-14)

<csr-id-711df9062f749cd331d799d4647d9a4988756816/>

### New Features

 - <csr-id-8c1b9f4e8c44ae61f2382c501284df6fbf03a4f3/> can successfully format it into slides with ``----```between slides
 - <csr-id-22d4ab7c2fa3c5e82f3d024bcd68067a8f831ec7/> getting frontmatter to work better
 - <csr-id-b267435ea00d0b84d867b56f5b8e43bd1382a012/> frontmatter can now accept slides as a parameter

### Bug Fixes

 - <csr-id-436e0b738e15cabe47903368b6f826ebcf7ab935/> set default of draft to false, had false positives
 - <csr-id-b67447a31d764b7de4c3a8d351384c8fd88f417d/> add a class to the slides
 - <csr-id-f818b34e848348685f7905b1b654841a6b74e468/> tidy this uo to be easier to understand and expandable

### Other

 - <csr-id-711df9062f749cd331d799d4647d9a4988756816/> clippy has some good ideas

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 8 commits contributed to the release.
 - 107 days passed between releases.
 - 7 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.45, bfom v0.1.39, bfom-blog v0.1.17 (f118c44)
    - Clippy has some good ideas (711df90)
    - Set default of draft to false, had false positives (436e0b7)
    - Add a class to the slides (b67447a)
    - Can successfully format it into slides with ``----```between slides (8c1b9f4)
    - Getting frontmatter to work better (22d4ab7)
    - Frontmatter can now accept slides as a parameter (b267435)
    - Tidy this uo to be easier to understand and expandable (f818b34)
</details>

## v0.1.44 (2022-12-28)

<csr-id-c6e4c04379cd79f46b1b5680ff9d613049ac3243/>
<csr-id-355bfded373550dd5d9642bb9319c1e4f5536658/>

### Other

 - <csr-id-c6e4c04379cd79f46b1b5680ff9d613049ac3243/> fmt and clippy
 - <csr-id-355bfded373550dd5d9642bb9319c1e4f5536658/> changed the fmt config to make stuff easier to read

### New Features

 - <csr-id-d103780284af85666f067cd77738d31da5c428d9/> will no longer render files with draft on tehm
 - <csr-id-c0a625f183f3a99d8a19a6f58ae3b0fd874b4ab7/> incorporate FrontMatter into teh basic convertor
 - <csr-id-798f4cef6cfe59a1e02aa91e1c4cfacf172240b7/> basic setup for FrontMatter complete

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 6 commits contributed to the release.
 - 198 days passed between releases.
 - 5 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.44, bfom v0.1.38, bfom-blog v0.1.15 (0813d16)
    - Fmt and clippy (c6e4c04)
    - Will no longer render files with draft on tehm (d103780)
    - Incorporate FrontMatter into teh basic convertor (c0a625f)
    - Changed the fmt config to make stuff easier to read (355bfde)
    - Basic setup for FrontMatter complete (798f4ce)
</details>

## v0.1.43 (2022-06-12)

### Bug Fixes

 - <csr-id-6c6593e47d5ca278ba328950fd3421644af02d8a/> was not updating teh inline text of lists properly

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 2 commits contributed to the release.
 - 72 days passed between releases.
 - 1 commit was understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.43 (18b6047)
    - Was not updating teh inline text of lists properly (6c6593e)
</details>

## v0.1.42 (2022-04-01)

### Bug Fixes

 - <csr-id-240141787daf00e979414b3798cb4c1bb5016fef/> now copies all files except for those forbidden

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 2 commits contributed to the release.
 - 1 commit was understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.42, bfom-blog v0.1.6 (6a2b6da)
    - Now copies all files except for those forbidden (2401417)
</details>

## v0.1.41 (2022-04-01)

<csr-id-1fbfe100825ff4b895069940f3f2b8d5bcf4943f/>

### Other

 - <csr-id-1fbfe100825ff4b895069940f3f2b8d5bcf4943f/> fmt and clippy

### New Features

 - <csr-id-42eaa856e3de9b3e1096198cc7620a85c2629bc5/> modified to accept range of filetypes

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 3 commits contributed to the release.
 - 2 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.41, bfom-blog v0.1.5 (6d12b58)
    - Fmt and clippy (1fbfe10)
    - Modified to accept range of filetypes (42eaa85)
</details>

## v0.1.40 (2022-03-31)

### New Features

 - <csr-id-a757e7676751aed0a8190b470390266390d98fde/> can now copy over any filetyoe required while also excluding certain directories
 - <csr-id-5e118054916bee038270d327c0d796625f11112b/> should be able to create lists without creating paragraps in them

### Bug Fixes

 - <csr-id-8245d2f3a60397dbec17487cfc433892e1235148/> minor change to bump
 - <csr-id-dfe8f62d635039c06cd083381a975d66cc04399a/> bump the version

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 5 commits contributed to the release over the course of 10 calendar days.
 - 63 days passed between releases.
 - 4 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.40, bfom-blog v0.1.4 (3cab792)
    - Can now copy over any filetyoe required while also excluding certain directories (a757e76)
    - Minor change to bump (8245d2f)
    - Bump the version (dfe8f62)
    - Should be able to create lists without creating paragraps in them (5e11805)
</details>

## v0.1.38 (2022-01-27)

### Bug Fixes

 - <csr-id-492b7baae4c381636904a85bd1bf74589222187e/> fixing some formatting

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 2 commits contributed to the release.
 - 1 commit was understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.38, bfom v0.1.37, bfom-blog v0.1.3 (d9f4f61)
    - Fixing some formatting (492b7ba)
</details>

## v0.1.37 (2022-01-27)

<csr-id-71e3a840b66b75d9c5aae8a95a7fa106e4222766/>

### Other

 - <csr-id-71e3a840b66b75d9c5aae8a95a7fa106e4222766/> going to ttry out teh docs from the tool

### Bug Fixes

 - <csr-id-dbc2ffbbc6763926fd68ec94a0e8017f39e337bc/> for bumping the package
 - <csr-id-7972d63dec99cf5b3ea5b7f268b91e680483d998/> for bumping the package
 - <csr-id-73c430c5aab75e36d7c9d84343e0e9b45c5ee6f7/> for bumping the package for testing

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 8 commits contributed to the release over the course of 5 calendar days.
 - 5 days passed between releases.
 - 4 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.37, bfom v0.1.36, bfom-blog v0.1.2 (eaf2e2a)
    - For bumping the package for testing (73c430c)
    - Bump bfom-lib v0.1.37, bfom v0.1.36, bfom-blog v0.1.2 (b92d078)
    - For bumping the package (7972d63)
    - Bump bfom-lib v0.1.37, bfom v0.1.36, bfom-blog v0.1.2 (dade144)
    - For bumping the package (dbc2ffb)
    - Bump bfom-lib v0.1.37, bfom v0.1.36, bfom-blog v0.1.1 (4a70751)
    - Going to ttry out teh docs from the tool (71e3a84)
</details>

## v0.1.36 (2022-01-22)

<csr-id-cd873370f04d13feff3aa8fe2d629edae24d1904/>

### Other

 - <csr-id-cd873370f04d13feff3aa8fe2d629edae24d1904/> improved the formatting

### New Features

 - <csr-id-76709b54674db955e30f809fb3653bffd1984d36/> split up the general conversion and my own blog, will allow me to customise the blog as much as I like

### Bug Fixes

 - <csr-id-4a96393f56f8989c0d4a9a9b193177f2929ac523/> switching to 2021 seems to be fine

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 6 commits contributed to the release.
 - 124 days passed between releases.
 - 3 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.36, bfom v0.1.35, bfom-blog v0.1.0 (efc92c2)
    - Improved the formatting (cd87337)
    - Switching to 2021 seems to be fine (4a96393)
    - Split up the general conversion and my own blog, will allow me to customise the blog as much as I like (76709b5)
    - Fmt (bfba0c4)
    - Merge branch 'general_improvements' (9e0c312)
</details>

## v0.1.35 (2021-09-19)

### New Features

 - <csr-id-fbaab53960f76929fb584d2628d72e5e9fe26299/> added some basic tests
 - <csr-id-7b27b91a5732a1af1768876db64069970991f1b9/> split line_data into its own module
 - <csr-id-8a3e0015f95abc21e7419391b23c6a590bcfc134/> successfully changed linetype to being an enum.
   Overall this does make it far neater and intuitive.
 - <csr-id-651dd2385359c917e63a051273a31a92d1b26f09/> lists now work on teh new line type function/enum
 - <csr-id-5bd5161ad0b6e359aa8d441299dc2b140e08fe0e/> better fmt options
 - <csr-id-bc216171cde11b8c0f024cff43d583e69ac8418f/> first step into using enums

### Bug Fixes

 - <csr-id-c7543a49ba144edab909572e31f83a208017a72e/> removed unused varible
 - <csr-id-da426ba981a03c03ebbbbce1d8c73049bd5f7f59/> no longer using cargo release, now using cargo smart-release as it can handle modules with different versions easier

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 9 commits contributed to the release over the course of 23 calendar days.
 - 23 days passed between releases.
 - 8 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Release bfom-lib v0.1.35 (01acb87)
    - Added some basic tests (fbaab53)
    - Removed unused varible (c7543a4)
    - Split line_data into its own module (7b27b91)
    - Successfully changed linetype to being an enum. (8a3e001)
    - Lists now work on teh new line type function/enum (651dd23)
    - Better fmt options (5bd5161)
    - First step into using enums (bc21617)
    - No longer using cargo release, now using cargo smart-release as it can handle modules with different versions easier (da426ba)
</details>

## v0.1.34 (2021-08-27)

<csr-id-6e557b02b81a0e68393da854aeda69e555e355d4/>
<csr-id-f1624e172dc3cc053097c2d0315abe29d1d07f65/>

### Test

 - <csr-id-6e557b02b81a0e68393da854aeda69e555e355d4/> releasing updated lib
 - <csr-id-f1624e172dc3cc053097c2d0315abe29d1d07f65/> bumping both subprojects

### New Features

 - <csr-id-3e4a4df6686e4874a3999d1bc152ca7e705b6949/> split out the converter even further
 - <csr-id-ab42a61597fcdb78a2293e86d261d00691a4ce76/> split out the converter

### Bug Fixes

 - <csr-id-78bf37f3d82b912b55dca315e6160d46780a312d/> a clean starting point for testing to resume on
 - <csr-id-ccd3b5090c4624c6aecc2e6269bf57b1a8b80bb9/> properly handle < and > in inline code blocks
 - <csr-id-c6ac74b58d8ebeb78971d520b213a0b29ce6024d/> only do one tag

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 13 commits contributed to the release over the course of 19 calendar days.
 - 19 days passed between releases.
 - 7 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Release bfom-lib v0.1.34 (9e04fe9)
    - Releasing updated lib (6e557b0)
    - Test; bump to 33 for more testing of cargo workspaces (d9d16c2)
    - A clean starting point for testing to resume on (78bf37f)
    - Bumping both subprojects (f1624e1)
    - V0.1.28 (c369692)
    - Properly handle < and > in inline code blocks (ccd3b50)
    - V0.1.27 (7ca6809)
    - Split out the converter even further (3e4a4df)
    - Split out the converter (ab42a61)
    - V0.1.26 (d7cf1de)
    - V0.1.25 (dfbaf00)
    - Only do one tag (c6ac74b)
</details>

## v0.1.24 (2021-08-07)

<csr-id-f24a63bfb966b44d49d9961de3c051273e3f44ad/>
<csr-id-dae8f924fbc0d494a2a0d8d934ae160dc13e791c/>

### Other

 - <csr-id-f24a63bfb966b44d49d9961de3c051273e3f44ad/> updated result
 - <csr-id-dae8f924fbc0d494a2a0d8d934ae160dc13e791c/> updated all the readme's

### New Features

 - <csr-id-7a4c8d9bc9f23d2c1fb7d3ade29196accca82e3c/> centralising the use of cargo release
 - <csr-id-1a78feea1f27784ed9787b0cde4e707fcf2c317b/> split out the project into a lib and binary, each having their own dependencies

### Bug Fixes

 - <csr-id-6105645b5c516c80e60172c322acf66e4f142f00/> make sure each sub project has its own license files

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 7 commits contributed to the release.
 - 5 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - V0.1.24 (0e4f731)
    - V0.1.23 (b4e9c92)
    - Centralising the use of cargo release (7a4c8d9)
    - Updated result (f24a63b)
    - Updated all the readme's (dae8f92)
    - Make sure each sub project has its own license files (6105645)
    - Split out the project into a lib and binary, each having their own dependencies (1a78fee)
</details>

