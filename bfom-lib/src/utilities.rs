pub mod processing {
  use std::{
    fs,
    io::{self},
    path::Path,
    path::PathBuf,
  };

  pub fn file_find_markdown(dir: &Path) -> io::Result<Vec<PathBuf>> {
    let mut markdown_files: Vec<PathBuf> = vec![];
    if dir.is_dir() {
      for entry in fs::read_dir(dir)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_dir() {
          let sub_files = self::file_find_markdown(&path)?;
          markdown_files.extend(sub_files);
        } else {
          // check if it ends with .md
          if let Some(extension) = path.extension() {
            if extension == "md" {
              markdown_files.push(path)
            }
          }
        }
      }
    }
    Ok(markdown_files)
  }

  pub fn copy_css(src: &Path, dest: &Path) -> io::Result<()> {
    copy_files(src, dest, &["md"], &[])
  }

  pub fn copy_files(src: &Path, dest: &Path, forbidden_files: &[&str], forbidden: &[PathBuf]) -> io::Result<()> {
    if src.is_dir() {
      if forbidden.contains(&(src.to_path_buf())) {
        return Ok(());
      }

      for entry in fs::read_dir(src)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_dir() {
          let mut dest_new = PathBuf::new();
          dest_new.push(dest);
          dest_new.push(entry.file_name());
          self::copy_files(&path, &dest_new, forbidden_files, forbidden)?;
        } else {
          // check if it ends with .css
          if let Some(extension) = path.extension() {
            if !forbidden_files.contains(&extension.to_str().unwrap_or_default()) {
              // grab the file

              let dest = if let Ok(stripped) = path.clone().strip_prefix(src) {
                Path::new(dest).join(stripped)
              } else {
                // if the prefix cannot be stripped then put in the output files beside teh in
                // though considering that its based of of src this should never be called
                path.clone()
              };

              // make sure it has a folder ot go into
              if let Some(parent) = dest.parent() {
                fs::create_dir_all(parent)?
              }

              fs::copy(&path, &dest)?;
            }
          }
        }
      }
    }
    Ok(())
  }

  // https://stackoverflow.com/a/38406885/11964934
  pub fn uppercase_first_letter(s: &str) -> String {
    let mut c = s.chars();
    match c.next() {
      None => String::new(),
      Some(f) => f.to_uppercase().collect::<String>() + c.as_str(),
    }
  }
}
