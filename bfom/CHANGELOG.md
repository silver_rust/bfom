# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## v0.1.47 (2024-08-12)

### Bug Fixes

 - <csr-id-7227c34cbb17d4c94fd3aac722281ea1f20c29fc/> properly use teh template root

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 1 commit contributed to the release.
 - 1 commit was understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Properly use teh template root (7227c34)
</details>

## v0.1.46 (2024-08-12)

### Bug Fixes

 - <csr-id-a4a885bc9e5c0d15a73ed9675a32152e4747caa3/> remove debug logging

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 2 commits contributed to the release.
 - 1 commit was understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom v0.1.46 (3b75471)
    - Remove debug logging (a4a885b)
</details>

## v0.1.45 (2024-08-12)

### New Features

 - <csr-id-7fdd6ea2b52f9bda6eee4d7d1cb40c79f549c8e4/> allow use of a non numeric slug

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 2 commits contributed to the release.
 - 1 commit was understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.52, bfom v0.1.45 (f00a233)
    - Allow use of a non numeric slug (7fdd6ea)
</details>

## v0.1.44 (2024-08-11)

<csr-id-6d23526a434c2be9c048d5f8219c004e1ed9db1d/>
<csr-id-d995e2d65dc536c3b184ea02f7ba3a2b5fab5da1/>

### New Features

 - <csr-id-7454a07696fd05cadceca05f34b447383b9e9741/> main program can now do the index functions
 - <csr-id-371182769de702e755bd52975fa89ba4839bd134/> removed the duplication in structs for teh configs

### Bug Fixes

 - <csr-id-5b1bb7c4430d74b35041185843807b1ee6c7eb7f/> sort teh roots so tehre are no namespace clashes

### Other

 - <csr-id-6d23526a434c2be9c048d5f8219c004e1ed9db1d/> changed over to calling it the index instead of blog to better reflect its goal
 - <csr-id-d995e2d65dc536c3b184ea02f7ba3a2b5fab5da1/> cleanded up segments which are no longer used

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 6 commits contributed to the release over the course of 7 calendar days.
 - 107 days passed between releases.
 - 5 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.51, bfom v0.1.44 (f7eaeaf)
    - Sort teh roots so tehre are no namespace clashes (5b1bb7c)
    - Changed over to calling it the index instead of blog to better reflect its goal (6d23526)
    - Cleanded up segments which are no longer used (d995e2d)
    - Main program can now do the index functions (7454a07)
    - Removed the duplication in structs for teh configs (3711827)
</details>

## v0.1.43 (2024-04-26)

### New Features

 - <csr-id-cd4690943521a6e02f90040b59cefc0522c5fada/> bump toml

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 2 commits contributed to the release.
 - 1 commit was understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom v0.1.43 (b9519c9)
    - Bump toml (cd46909)
</details>

## v0.1.42 (2024-04-26)

### Bug Fixes

 - <csr-id-d699f4c5bcfa26e791835bcd6fda93349d10db12/> remove excess logging

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 2 commits contributed to the release.
 - 214 days passed between releases.
 - 1 commit was understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom v0.1.42 (ff88e85)
    - Remove excess logging (d699f4c)
</details>

## v0.1.41 (2023-09-24)

### New Features

 - <csr-id-3402f408f7921fc0634a5df3f526a341d3e09e0a/> better handling of general powerpoints

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 2 commits contributed to the release.
 - 144 days passed between releases.
 - 1 commit was understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.48, bfom v0.1.41, bfom-blog v0.1.20 (12301d7)
    - Better handling of general powerpoints (3402f40)
</details>

## v0.1.40 (2023-05-02)

### Bug Fixes

 - <csr-id-5a3f7625723591e9b7518921624e2e7c3326e6a9/> update both packages to use the fix from #4

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 2 commits contributed to the release.
 - 17 days passed between releases.
 - 1 commit was understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom v0.1.40, bfom-blog v0.1.19 (35dcae0)
    - Update both packages to use the fix from #4 (5a3f762)
</details>

## v0.1.39 (2023-04-14)

<csr-id-711df9062f749cd331d799d4647d9a4988756816/>

### New Features

 - <csr-id-a1ade11c60c752c69ce9b4b8b7350fbe77a2d8e1/> css and js for slides is now built in, maybe not the best way to do it but good enough
 - <csr-id-22d4ab7c2fa3c5e82f3d024bcd68067a8f831ec7/> getting frontmatter to work better

### Bug Fixes

 - <csr-id-299c22ac2032748248c564952f44f49bbb09c898/> reduced the complexity of the default html

### Other

 - <csr-id-711df9062f749cd331d799d4647d9a4988756816/> clippy has some good ideas

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 5 commits contributed to the release.
 - 107 days passed between releases.
 - 4 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.45, bfom v0.1.39, bfom-blog v0.1.17 (f118c44)
    - Clippy has some good ideas (711df90)
    - Css and js for slides is now built in, maybe not the best way to do it but good enough (a1ade11)
    - Reduced the complexity of the default html (299c22a)
    - Getting frontmatter to work better (22d4ab7)
</details>

## v0.1.38 (2022-12-28)

<csr-id-c6e4c04379cd79f46b1b5680ff9d613049ac3243/>
<csr-id-355bfded373550dd5d9642bb9319c1e4f5536658/>

### Other

 - <csr-id-c6e4c04379cd79f46b1b5680ff9d613049ac3243/> fmt and clippy
 - <csr-id-355bfded373550dd5d9642bb9319c1e4f5536658/> changed the fmt config to make stuff easier to read

### New Features

 - <csr-id-c0a625f183f3a99d8a19a6f58ae3b0fd874b4ab7/> incorporate FrontMatter into teh basic convertor
 - <csr-id-798f4cef6cfe59a1e02aa91e1c4cfacf172240b7/> basic setup for FrontMatter complete

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 5 commits contributed to the release.
 - 334 days passed between releases.
 - 4 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.44, bfom v0.1.38, bfom-blog v0.1.15 (0813d16)
    - Fmt and clippy (c6e4c04)
    - Incorporate FrontMatter into teh basic convertor (c0a625f)
    - Changed the fmt config to make stuff easier to read (355bfde)
    - Basic setup for FrontMatter complete (798f4ce)
</details>

## v0.1.37 (2022-01-27)

### Bug Fixes

 - <csr-id-492b7baae4c381636904a85bd1bf74589222187e/> fixing some formatting

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 2 commits contributed to the release.
 - 1 commit was understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.38, bfom v0.1.37, bfom-blog v0.1.3 (d9f4f61)
    - Fixing some formatting (492b7ba)
</details>

## v0.1.36 (2022-01-27)

<csr-id-71e3a840b66b75d9c5aae8a95a7fa106e4222766/>

### Other

 - <csr-id-71e3a840b66b75d9c5aae8a95a7fa106e4222766/> going to ttry out teh docs from the tool

### Bug Fixes

 - <csr-id-dbc2ffbbc6763926fd68ec94a0e8017f39e337bc/> for bumping the package
 - <csr-id-7972d63dec99cf5b3ea5b7f268b91e680483d998/> for bumping the package
 - <csr-id-73c430c5aab75e36d7c9d84343e0e9b45c5ee6f7/> for bumping the package for testing

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 8 commits contributed to the release over the course of 5 calendar days.
 - 5 days passed between releases.
 - 4 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.37, bfom v0.1.36, bfom-blog v0.1.2 (eaf2e2a)
    - For bumping the package for testing (73c430c)
    - Bump bfom-lib v0.1.37, bfom v0.1.36, bfom-blog v0.1.2 (b92d078)
    - For bumping the package (7972d63)
    - Bump bfom-lib v0.1.37, bfom v0.1.36, bfom-blog v0.1.2 (dade144)
    - For bumping the package (dbc2ffb)
    - Bump bfom-lib v0.1.37, bfom v0.1.36, bfom-blog v0.1.1 (4a70751)
    - Going to ttry out teh docs from the tool (71e3a84)
</details>

## v0.1.35 (2022-01-22)

<csr-id-cd873370f04d13feff3aa8fe2d629edae24d1904/>

### Other

 - <csr-id-cd873370f04d13feff3aa8fe2d629edae24d1904/> improved the formatting

### New Features

 - <csr-id-76709b54674db955e30f809fb3653bffd1984d36/> split up the general conversion and my own blog, will allow me to customise the blog as much as I like

### Bug Fixes

 - <csr-id-4a96393f56f8989c0d4a9a9b193177f2929ac523/> switching to 2021 seems to be fine
 - <csr-id-3ffabfa7558e8867c907c80106663ce40b539f50/> feckin fmt

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 6 commits contributed to the release over the course of 124 calendar days.
 - 124 days passed between releases.
 - 4 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Bump bfom-lib v0.1.36, bfom v0.1.35, bfom-blog v0.1.0 (efc92c2)
    - Improved the formatting (cd87337)
    - Switching to 2021 seems to be fine (4a96393)
    - Split up the general conversion and my own blog, will allow me to customise the blog as much as I like (76709b5)
    - Merge branch 'general_improvements' (9e0c312)
    - Feckin fmt (3ffabfa)
</details>

## v0.1.34 (2021-09-19)

<csr-id-f1624e172dc3cc053097c2d0315abe29d1d07f65/>
<csr-id-63788e34fdb388a02e370983d7790cf5b7f983dc/>
<csr-id-dae8f924fbc0d494a2a0d8d934ae160dc13e791c/>
<csr-id-6d540eb1b98bdc9cabd873c1c6e5aa82d8f91bfb/>

### Test

 - <csr-id-f1624e172dc3cc053097c2d0315abe29d1d07f65/> bumping both subprojects

### Other

 - <csr-id-63788e34fdb388a02e370983d7790cf5b7f983dc/> updated links back tot he root
 - <csr-id-dae8f924fbc0d494a2a0d8d934ae160dc13e791c/> updated all the readme's
 - <csr-id-6d540eb1b98bdc9cabd873c1c6e5aa82d8f91bfb/> moved the documentation into the appropiate folder

### New Features

 - <csr-id-f6400aa09a8f5df60ded72988d4ce1cd4694f0e7/> get the folder a file is in and fill that in the template.
 - <csr-id-cb8af8bcc7f18c4a95b45f2089ff0088c1b7891e/> get the folder a file is in and fill that in the template.
 - <csr-id-5bd5161ad0b6e359aa8d441299dc2b140e08fe0e/> better fmt options
 - <csr-id-3e4a4df6686e4874a3999d1bc152ca7e705b6949/> split out the converter even further
 - <csr-id-ab42a61597fcdb78a2293e86d261d00691a4ce76/> split out the converter
 - <csr-id-7a4c8d9bc9f23d2c1fb7d3ade29196accca82e3c/> centralising the use of cargo release
 - <csr-id-1a78feea1f27784ed9787b0cde4e707fcf2c317b/> split out the project into a lib and binary, each having their own dependencies

### Bug Fixes

 - <csr-id-da426ba981a03c03ebbbbce1d8c73049bd5f7f59/> no longer using cargo release, now using cargo smart-release as it can handle modules with different versions easier
 - <csr-id-78bf37f3d82b912b55dca315e6160d46780a312d/> a clean starting point for testing to resume on
 - <csr-id-d192658fc5cf0c10f0ab9df8d0eb7ae4face2c84/> update versions for crates.io
 - <csr-id-2a3f684128fec5b48f573561e371ce5f8b275a90/> now creates the proper tag formats
 - <csr-id-6105645b5c516c80e60172c322acf66e4f142f00/> make sure each sub project has its own license files

### Commit Statistics

<csr-read-only-do-not-edit/>

 - 24 commits contributed to the release over the course of 43 calendar days.
 - 16 commits were understood as [conventional](https://www.conventionalcommits.org).
 - 0 issues like '(#ID)' were seen in commit messages

### Commit Details

<csr-read-only-do-not-edit/>

<details><summary>view details</summary>

 * **Uncategorized**
    - Release bfom v0.1.34 (cf83534)
    - Get the folder a file is in and fill that in the template. (f6400aa)
    - Get the folder a file is in and fill that in the template. (cb8af8b)
    - Better fmt options (5bd5161)
    - No longer using cargo release, now using cargo smart-release as it can handle modules with different versions easier (da426ba)
    - Test; bump to 33 for more testing of cargo workspaces (d9d16c2)
    - A clean starting point for testing to resume on (78bf37f)
    - Bumping both subprojects (f1624e1)
    - V0.1.28 (d840835)
    - V0.1.27 (722c67b)
    - Split out the converter even further (3e4a4df)
    - Split out the converter (ab42a61)
    - V0.1.26 (ec91c74)
    - Update versions for crates.io (d192658)
    - V0.1.25 (6fce939)
    - V0.1.24 (a2a7029)
    - Now creates the proper tag formats (2a3f684)
    - V0.1.23 (d5ec1f4)
    - Centralising the use of cargo release (7a4c8d9)
    - Updated links back tot he root (63788e3)
    - Updated all the readme's (dae8f92)
    - Moved the documentation into the appropiate folder (6d540eb)
    - Make sure each sub project has its own license files (6105645)
    - Split out the project into a lib and binary, each having their own dependencies (1a78fee)
</details>

