# Brendan's Flavor of Markdown
##### CLI program

## Usage

### Install
Crates.io: ``cargo install bfom``

### Running it
it can be run with ``cargo bfom``.  
The defaults can be overwritten by having a ``.md.toml`` in the same directory the program is run in.  
An example of this can be found [here](./example.md.toml).

## About
### Overview
An overview is available at the root of this project: [Main Readme](../README.md)

### Background
If you want to read about the rabbit-hole I fell down please direct your attention to: [background].

### Changes
It builds off of existing markdown but has some significant changes.  
If you want to see the actual implementation please look at [breaking changes][changes].


[background]: https://brendan.ie/blog/2 "Descent into Madness"
[changes]: ./BFoM.md "So deep that the way up is down and through"