# Brendan's Flavor of Markdown
##### Overview

## Subcomponents

CLI tool: [BFoM]  
Library: [BFoM library]

[BFoM]: ./bfom
[BFoM library]: ./bfom-lib


## About
I had decided to do a markdown converter as a small summer project in order to gain more experience with rust.  
It has taken far longer than I thought it would, in part due to excessive (for Ireland) heat but mostly due to the rabbit-hole that Markdown ended up being.

The end result is I ended up creating my own flavor of markdown.  
It is both stricter than Commonmark, yet at the same time more permissive.

Stricter:
* ``__`` is no longer used for ``<strong>``, only ``**`` is.
* Lazy continuation is disallowed.
* Inline spans now have double identifiers.
    * ``<`` and ``>`` to ``<<`` and ``>>``
    * The same happens with code spans.

Permissive:
* Extensions:
    * Tables.
    * Spoilers.
    * Strikethrough.
* Leading whitespace no longer triggers a code block, so it is now possible to properly align headers.


I honestly don't expect anyone besides myself to use it.  
The existing flavors have been well established, and I dont have the clout of Gitlab/Github/Bitbucket/Reddit to get folks to use it.  
Not only that, this converter is currently a mess I have to split up for readability.  
I am also pretty sure that there are some nuanced bugs in it however I am pretty sure the idea behind the changes is sound.

As a summer project thought it is mostly successful, I have gained a decent bit more proficiency with rust.  
The conversion has no external dependencies (serde and toml are to read the optional config files).  
It also is the first crate I have pushed to crates.io.

### Background
If you want to read about the rabbit-hole I fell down please direct your attention to: [background].

### Changes
It builds off of existing markdown but has some significant changes.  
If you want to see the actual implementation please look at [breaking changes][changes].


[background]: https://brendan.ie/blog/2 "Descent into Madness"
[changes]: ./bfom/BFoM.md "So deep that the way up is down and through"

### With thanks to
##### Alain Zscheile
He made a lot of suggestions on the [merge request](https://gitlab.com/silver_rust/bfom/-/merge_requests/1), most of which were beyond my own ability at the time.